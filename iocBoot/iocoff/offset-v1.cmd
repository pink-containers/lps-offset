#!../../bin/linux-x86_64/off

## You may have to change off to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/off.dbd"
off_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

dbLoadRecords("energy.db", "BL=$(IOCBL),DEV=$(IOCDEV)")
dbLoadTemplate("offset.subs", "BL=$(IOCBL), DEV=$(IOCDEV)")

## autosave
set_savefile_path("/EPICS/autosave")
set_requestfile_path("${TOP}/iocBoot/${IOC}")
set_pass0_restoreFile("auto_settings.sav")
#set_pass1_restoreFile("auto_settings.sav")

iocInit

## autosave
create_monitor_set("auto_settings.req", 30, "BL=$(IOCBL), DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
